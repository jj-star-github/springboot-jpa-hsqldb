package com.dingjunjun.user;

import com.dingjunjun.UserApplication;
import com.dingjunjun.pojo.User;
import com.dingjunjun.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Description:
 * @Version: V1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UserApplication.class)
public class UserTest {

    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext ctx;

    private MockMvc mockMvc;

    /**
     * 初始化 MVC 的环境
     */
    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }



    /**
     * 添加user信息
     */
    @Test
    public void addUser() {
        User user = new User();
        user.setId(1);
        user.setName("丁军军");
        user.setBirth(new Date());
        user.setOnWork(true);
        userService.saveUser(user);
    }


    /**
     * 查询现有的所有user的信息
     */
    @Test
    public void userFindAll() {
        System.out.println(userService.userFindAll());
    }

    /**
     * 查询现有的所有user的信息
     * @throws Exception
     */
    public void userFindAllTest() throws Exception {
        this.mockMvc.perform(get("/userFindAll"))
                .andExpect(status().isCreated());
    }
}
