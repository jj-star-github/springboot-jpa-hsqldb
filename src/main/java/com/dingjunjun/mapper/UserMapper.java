package com.dingjunjun.mapper;

import com.dingjunjun.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
public interface UserMapper extends JpaRepository<User, Integer> {
}
