package com.dingjunjun.common.knife4j;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Configuration
//@EnableSwagger2 Swagger2API文档的配置,swagger2 3.x时代，注解@EnableSwagger2就不需要了  访问:.../swagger-ui.html
@EnableKnife4j //直接访问 .../doc.html
public class Swagger2Config {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                // 指定构建api文档的详细信息的方法：apiInfo()
                .apiInfo(apiInfo())
                .select()
                // 指定要生成api接口的包路径，这里把controller作为包路径，生成controller中的所有接口
                .apis(RequestHandlerSelectors.basePackage("com.dingjunjun.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 设置页面标题
                .title("Springboot集成HSQLDB和JPA")
                // 设置接口描述
                .description("Springboot集成HSQLDB和JPA的API")
                // 设置联系方式
                .contact(new Contact("dingjunjun", null, "dingjunjun163f@163.com"))
                // 设置版本
                .version("1.0")
                .build();
    }
}
