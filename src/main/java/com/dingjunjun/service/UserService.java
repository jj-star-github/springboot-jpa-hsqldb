package com.dingjunjun.service;

import com.alibaba.fastjson.JSON;
import com.dingjunjun.mapper.UserMapper;
import com.dingjunjun.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Service
@Slf4j
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserMapper userMapper;

    /**
     * 添加用户
     *
     * @param user
     */
    public void saveUser(User user) {
        LOGGER.info("into method saveUser,入参:" + JSON.toJSONString(user));
        user.setId((int) Math.random());
        userMapper.save(user);
    }


    /**
     * 查询所有信息
     *
     * @return
     */
    public List<User> userFindAll() {
        LOGGER.info("into method findUserById：");
        return userMapper.findAll();
    }

    /**
     * 根据id查询user信息
     *
     * @param id
     * @return
     */
    public User findUserById(Integer id) {
        LOGGER.info("into method findUserById,入参:" + JSON.toJSONString(id));
        return userMapper.findById(id).get();
    }


    /**根据id批量查询
     *
     * @param idList
     * @return
     */
    public List<User> batchFindUserByIds(List<Integer> idList) {
        LOGGER.info("into method batchFindUserByIds,入参:" + JSON.toJSONString(idList));
        return userMapper.findAllById(idList);
    }



    /**
     * 修改更新用户
     *
     * @param user
     * @return
     */
    public User updateUser(User user) {
        LOGGER.info("into method updateUser,入参:" + JSON.toJSONString(user));
        if (null != user) {
            User checkUser = this.findUserById(user.getId());
            if (null != checkUser) {
                return userMapper.save(user);
            } else {
                log.info("输入查询的对象不能为空！");
            }
        }
        return user;
    }
}
