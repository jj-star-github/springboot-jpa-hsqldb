package com.dingjunjun.service.jobhandler;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 任务Handler示例（Bean模式）
 * 开发步骤：
 * 1、继承"IJobHandler"：“com.xxl.job.core.handler.IJobHandler”；
 * 2、注册到Spring容器：添加“@Component”注解，被Spring容器扫描为Bean实例；
 * 3、注册到执行器工厂：添加“@JobHandler(value="自定义jobhandler名称")”注解，注解value值对应的是调度中心新建任务的JobHandler属性的值。
 * 4、执行日志：需要通过 "XxlJobLogger.log" 打印执行日志；
 * 5、任务执行结果枚举：SUCCESS、FAIL、FAIL_TIMEOUT
 *
 * @author 22372
 */
@Component
public class DemoJobHandler {

    //private static Logger logger = LoggerFactory.getLogger(DemoJobHandler.class);

    @XxlJob("testJobHandler")
    public void testJobHandler() throws Exception{
        XxlJobHelper.log("XXL-JOB, testJobHandler！");

        for (int i = 0; i < 5; i++) {
            XxlJobHelper.log("beat at:"+i);
            TimeUnit.SECONDS.sleep(2);
        }
    }
}
