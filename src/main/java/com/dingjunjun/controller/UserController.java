package com.dingjunjun.controller;

import com.dingjunjun.common.entity.Result;
import com.dingjunjun.common.entity.StatusCode;
import com.dingjunjun.pojo.User;
import com.dingjunjun.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@RestController
@RequestMapping("/user")
@Api(tags = "Springboot集成HSQLDB和JPA")
public class UserController {

    @Autowired
    private UserService userService;
    /**
     * 添加user信息
     * @param user
     * @return
     */
    @ApiOperation(value = "添加单个user的信息",
            notes = "<span style='color:red;'>描述:</span>&nbsp;用来添加单个user的信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user",value = "user对象",dataType = "User",defaultValue = "dingjunjun")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "参数错误"),
            @ApiResponse(code = 404, message = "请求路径不正确")
    })
    @PostMapping("/addUser")
    public Result addUser(@RequestBody User user) {
        userService.saveUser(user);
        return new Result(true, StatusCode.OK,"添加成功！");
    }

    /**
     * 查询所有信息
     * @return
     */
    @ApiOperation(value = "查询所有user的信息",
            notes = "<span style='color:red;'>描述:</span>&nbsp;用来查询所有user的信息")
    @ApiResponses({
            @ApiResponse(code = 400, message = "参数错误"),
            @ApiResponse(code = 404, message = "请求路径不正确")
    })
    @GetMapping("/userFindAll")
    public Result userFindAll() {
        return new Result(true,StatusCode.OK,"查询所有成功！",userService.userFindAll());
    }

    /**
     * 根据id查询user信息
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询user信息",
            notes = "<span style='color:red;'>描述:</span>&nbsp;用来根据id查询user信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "user的id",dataType = "Integer",defaultValue = "1")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "参数错误"),
            @ApiResponse(code = 404, message = "请求路径不正确")
    })
    @GetMapping("/userFindById/{id}")
    public Result userFindById(@PathVariable Integer id) {
        return new Result(true, StatusCode.OK,"根据id查询成功！",userService.findUserById(id));
    }


    /**根据id批量查询
     *
     * @param idList
     * @return
     */
    @ApiOperation(value = "根据id批量查询",
            notes = "<span style='color:red;'>描述:</span>&nbsp;用来根据id批量查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "idList",value = "user的id集合",dataType = "List<Integer>",defaultValue = "dingjunjun")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "参数错误"),
            @ApiResponse(code = 404, message = "请求路径不正确")
    })
    @GetMapping("/batchFindUserByIds/{idList}")
    public Result batchFindUserByIds(@PathVariable List<Integer> idList) {
        return new Result(true, StatusCode.OK,"根据id批量查询成功！",userService.batchFindUserByIds(idList));
    }

    /**
     * 修改更新用户信息
     * @param user
     * @return
     */
    @ApiOperation(value = "修改更新用户信息",
            notes = "<span style='color:red;'>描述:</span>&nbsp;用来修改更新用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user",value = "user对象",dataType = "User",defaultValue = "dingjunjun")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "参数错误"),
            @ApiResponse(code = 404, message = "请求路径不正确")
    })
    @PutMapping("/updateUser")
    public Result updateUser(@RequestBody User user) {
        return new Result(true,StatusCode.OK,"更新用户信息成功！",userService.updateUser(user));
    }
}
