1.注意xxl-job-admin的版本  和   executor样例的版本一致性问题

2.在新的版本中，这里使的是version:2.3.0版本的，在XxlJobConfig中的xxlJobExecutor方法中的配置：
2.1：只用@Bean，就可以了，后面不用加“(initMethod = "start", destroyMethod = "destroy")”
2.2：在在XxlJobConfig的方法上，不再需要
//指定任务Handler所在包路径
@ComponentScan(basePackages = "com.example.mycloud.job") 
一个注解@component就够了

3.
3.1：在具体的执行定时任务的类中，不需要
@JobHandler(value="TestHandler")注解功能，
也不需要去继承IJobHandler()方法，只需要一个注解@component
3.2：在具体的执行定时任务的方法中，
只用加一个注解@XxlJob("testJobHandler")
括号里面是JobHandler的配置的名字


4.其他的application.properties配置问题，注意的就是，xxl-job-admin的地址要一致，
注意端口别被占用
